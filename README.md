# Terminal Pomodoro

![Alt Image text](https://gitlab.com/Gibsol/pomodoro-cli/-/raw/main/images/terminal%20pomodoro.png)


A basic Console Line Interface Pomodoro application written in C++.

## Compatibility
Currently the app runs only on Linux.

## Installation
1. Clone the repo.
2. Launch the pomodoro-cli.out file.  

## License
GNU General Public License v3.0.

## Project status
Ready to go.